\documentclass[a4paper, 11pt]{article}
\usepackage[utf8]{inputenc}
%\usepackage[croatian]{babel}
\usepackage[margin=18mm]{geometry}
\usepackage{setspace}
\onehalfspacing
\usepackage{amsmath} 
%\usepackage{braket} 
\usepackage{amssymb} 
\numberwithin{equation}{section}
\usepackage[font={small}]{caption}

\usepackage{float}

\usepackage{graphicx}
\usepackage{subcaption}
\usepackage[dvipsnames]{xcolor}

\date{}

\hyphenation{pergamon}

\setlength\parindent{0pt} 
\setlength{\parskip}{10pt} 

\setlength{\skip\footins}{2cm} 
\renewcommand{\thefootnote}{$\ddagger$}

\newcommand{\HRule}{\rule{\linewidth}{0.4mm}}

\title{\Huge \bfseries{Search for Higgs boson pair production in proton-proton collisions at the Large Hadron Collider at CERN}}
\setlength{\columnsep}{1.2cm} 

\DeclareMathOperator{\Lagr}{\mathcal{L}}

\begin{document}

\maketitle

\tableofcontents
\newpage

\section{Introduction}

The main theory of interactions in physics, the standard model (SM), was presented in the mid to late-1900s with a lot of assumptions which turned out to be correct and it gave very promising results in experiments so far. One of the key parts in the model is the Higgs particle whose existence has recently been confirmed in 2012 \cite{r1} \cite{r2} using the LHC (large hadron collider) at CERN. It is a known fact that the standard model is not complete and by further researching the properties of the Higgs boson there was hope that some indication of a property of the particle would be found beyond the standard model (BSM). As it turned out the properties were rather consistent with the SM predictions. \\
\\
In recent years, the idea persists that researching higgs pair production would maybe give more insight regarding the BSM. Such a process is rare, but it can be searched for in proton-proton collisions using the LHC at CERN. The main idea is that if the mode of production is resonant, by analyzing jets of particles in the detector that result from the decay of pairs of Higgs bosons and by using a reconstruction method, some information on the massive resonant particle could be acquired or rather some theory predictions for the particle could be tested. \\
\\
The most imporant decay channel of the Higgs boson that is used in the search is the decay to a pair of $b\bar{b}$ quarks because it is the most dominant one, so it will provide the most data for a given amount of events, that is, for a given number of bunch crossings which contain several proton-proton collisions. 

\subsection{Higgs Field Theory}

One of the most important and fundamental parts of SM field theory is scalar electrodinamics, in which all the fields are scalar fields. These scalar fields couple to gauge fields and provide the simplest frame to build the Lagrangian and define the Higgs mechanism. Scalar electrodynamics introduces the simplest form of the interaction between complex scalar fileds with spin 0 and massless spin 1 fields so that the Lagrangian is defined in this way:

\begin{equation}
\Lagr = -\dfrac{1}{4}F_{\mu\nu}F^{\mu\nu}+({\cal D}_\mu \phi)^\dagger ({\cal D}^\mu \phi)-V(\phi)
\end{equation}

where ${\cal D}$ is the covariant derivative and is equal to ${\cal D}=\partial_\mu + iqA_\mu$, which was created using the rules for transformation for vector fields and U(1) gauge group symmetry transformation rules. It can be shown that this above product of covariant derivative operators is also invariant under the local U(1) transformation, which gives a conclusion that the above Lagrangian for scalar electrodynamic is also invariant under such transformation (depending of the choice of the potential $V$ of course) \cite{r3}. For a specific choice of the potential that includes the $\phi^4$ term the global symmetry will be broken:

\begin{equation}
V(\phi)=-\mu^2 \phi^* \phi + \dfrac{\lambda}{2}(\phi^* \phi)^2
\end{equation}

The minimum of this potential can be calculated (in this case multiple vacuum states exist and they are all connected with the global U(1) symmetry), in fact it can be rewritten around the minimum so that the Lagrangian for scalar electrodynamics with the choice of the potential in the vacuum state will contain two different scalar fields in the theory, one will acquire mass and the other will be a massless Goldstone boson and by rewriting the Lagrangian in this way one would get a lot of terms with derivatives that determine interactions. To write the resulting Lagrangian more elegantly, invariance under U(1) gauge symmetry and invariance under gauge transformations is used so that the Goldstone boson field disappears and the massive field acquires real values \cite{r4}, i.e. the gauge boson in the theory became massive because of spontaneous symmetry breaking while the apparent field which represented the Goldstone boson was removed from the theory simply because of invariance of the Lagrangian and its terms while the physical spin degrees of freedom are still preserved within the theory. \\

\subsection{Self-Coupling Parameter}

Mass is a very important property of the Higgs boson, even though SM does predict almost all important properties of the particle(electric charge, parity, charge conjugation, branching ratios etc.) it does not predict the mass of the particle itself (or any properties that depend on the exact value of the mass). To determine the mass an exact measurement was necessary and as mentioned earlier the value for the mass was first obtained in 2012. at the LHC and by summing up the measurements until now the average value is approximately equal to $m_H=125.10 \pm 0.14$ GeV \cite{r5} \cite{r6} \cite{r7}. \\
\\
While the mass was determined, there is still another important parameter that the SM predicts and it is found in the definition of the potential of the Higgs boson, it is the self-coupling parameter $\lambda$. It is also very logical to assume that such a parameter must exist in the theory, because obviously a value of the Higgs particle mass was determined so the Higgs field must be able to give mass to itself, in other words it must interact with itself. As is the case with the mass, the exact value is not given by the theory so precise measurement is necessary. Recently only upper limits were determined by the CMS and ATLAS collaborations at LHC \cite{r8} \cite{r9}. 

\textcolor{red}{I will probably return to this section later with more information} 

\subsection{Production Modes}

To test a lot of predictions of different BSM models (for example predictions of particles that are not part of the SM but would contribute to solving existing problems with the SM) that have not been rejected yet, a measure of the production of Higgs boson pairs is necessary. From calculations in SM, it is known that the cross section for the production of a Higgs particle pair is very small due to the nature of perturbative QCD and Feynman diagram contributions. The current known possible ways of production are either through the decay of a resonant massive particle, although such a particle is not part of the SM, or through a non-resonant production mode.

Both processes in Fig. 1. involve loop interactions between quarks and are associated with gluon production which was also a part of the production of just one Higgs particle. Here the second diagram is particularly interesting because it features a vertex that is related to trilinear coupling of the Higgs particle which is directly related to the self-coupling parameter defined in Higgs field potential. It can be shown that those two diagrams are interfering destructively since their amplitudes are of similar order of magnitude which means that the cross section for the process will be greatly reduced. The diagrams are of course formed looking at the Lagrangian of the model, which means that the diagram, vertices and lines, will vary for SM and BSM models meaning that BSM might introduce possible contributions to the total diagram sum or rather the total invariant amplitude of the process resulting in change of the cross section value. \\
\\
In the first run of the LHC a lot of the possible decay channels associated with the di-Higgs decay were investigated, while more of the channels were added and the centre-of-mass energy of the collisions was increased in the second run and upper limis for the signal strengths ($95\%$ CL) were determined for most of them \cite{r10}. \textcolor{red}{More information later} \\

Non-resonant production modes are important for determining the self-coupling constant needed for Higgs self-interactions but unfortunately the SM does not provide a promising frame for it since production cross-sections are rather low. Because of that it is useful to look at resonant production modes (Fig. 2) that introduce BSM physics to solve the problem of low cross section values and they also propose potentional particles that would decay to a pair of Higgs bosons and would be observable by reconstruction in colliders like the LHC \cite{r11}.

\subsection{Warped Extra Dimensions, Bulk Graviton and the Radion}

One of the most interesting BSM models that predict the massive resonant particle are mainly extradimensional models with warped extradimensions. Recently a solution to the hierarchy problem was introduced in models that introduce the existance of multiple extra dimensions (usually one or two) that exist next to the 4 known space-time dimensions, where the extra dimensions could be very large and hidden or very small and hard to detect. It also introduced branes and proposed that the fundamental fields in SM physics and all matter would propagate through this four dimensional brane, while the gravitational force would be free to propagate through the other introduced multiple dimensions (the bulk). This would also result in a change in the known Minkowski metric (depending of the choice of number of extra dimensions) and it shows that by calculating Kaluza-Klein modes or by assuming that the compact additional dimensions are very large the hierarchy problem can be solved but new problems of hierarchy would soon arise \cite{r12} \cite{r13}. \\
\\
It seems that this problem can also be avoided by proposing a different kind of metric, one where the Minkowski space-time metric is multiplied by a warp factor which depends on properties of the additional dimensions \cite{r12}:

\begin{equation}
ds^2=e^{-2kr_{c}\phi}\eta_{\mu\nu}dx^{\mu}dx^{\nu}+r^{2}_{c}d\phi^2
\end{equation}

where $\phi$ is the coordinate of the extra dimension (it was shown that it is enough to define it from 0 to $\pi$), k is scale of Planck scale order of magnitude and $r_c$. With this choice of the metric and some assumptions it can be shown that Einstein's equations are solved and only one extra dimension is necessary to solve the hierarchy problem without new ones appearing, but most importantly the Kaluza-Klein excitations can be observed using existent methods in the particle colliders and a signal of a spin-2 particle, the bulk graviton, is expected in the TeV scale. The exact mass is not precisely predicted but it shows that masses below $~4$ TeV are disfavoured by electroweak and flavor precision tests \cite{r11}. \\
\\
The search for the spin-2 graviton is well motivated and finding proof of its existance would give significantly more importance to the warped dimension models, but other massive resonances should not be excluded yet, for example it is important to analyze one other model that also proposes a candidate for the potential massive particle. Since all considered models are connected to the Kaluza-Klein theory it is useful to write the space-time metric that proposed the fifth dimension at this point:

\renewcommand\arraystretch{2}
\begin{equation}
g^{KK}_{\mu\nu} =
\begin{bmatrix}
g_{\mu\nu+\phi^2A_{\mu}A_{\nu}} & \phi^2 A_{\mu} \\
\phi^2 A_{\nu} & \phi^2 
\end{bmatrix} 
\end{equation}

where $g_{\mu}{\nu}$ is the standard known four-dimensional metric, $A^\mu$ is the four vector of the electromagnetic potential. Introducing the fifth dimension a new scalar field $\phi$ appears in the metric which would lead to the existance of a new particle, the radion, due to the excitation of the quantum gravity field. It can be shown that by using this approach in a model that features only two branes and constructing a metric that is similar to that in the case of the graviton, thus solving the hierarchy problem. Fluctuations in the radion field are defined as fluctuations between the two proposed branes which are small for the static case and describe a massles particle that later acquires mass because of the coupling values, resulting in a spin-0 massive particle \cite{r14}. Since the radion field $\phi$ will acquire mass (and coupling values to other SM fields) which is also in the TeV scale, it means that it is possible to observe it in current or future particle collider processes. 

\subsection{Collider Kinematics}

To understand the process of the production of a pair of Higgs bosons it is important to understand the kinematics behind the process and the structure of the detector that will be used to detect that process. For this purpose important physical values used in particle colliders will be explained and defined. Even though calculation results do not depend on the structure of the detector, the results in the calculations are shown in terms of the physical quantities commonly used in high-energy colliders. Focusing mainly on the CMS (Compact Muon Solenoid) detector, parts of the detector can be divided into the point of collision (or interaction), where the magnets around the detector focus the colliding protons into one point of interaction, the tracker (which reconstructs the path of the products of the collision, mainly hadrons, electrons and muons), the electromagnetic calorimeter (where the energy from charged particles like electrons and muons is stored and measured with high precision) and the muon detector \cite{r15}. \\

For the purpose of generating a lot of collisions, which improves statistics and gives more information about the process a large number of protons are classified into bunches of approx. $1.2 \dot 10^11$ protons and are being collided. One LHC "bunch crossing" that contains several proton-proton collisions is called an event. With an interval of 25 ns between bunch crossings (also called bunch spacing), millions of proton collisions happen in one second. This multiplicity of collisions that is produced per unit of time and area is also summarised in the luminosity of the accelerator. The LHC has a large luminosity of $10^34$ cm$^{-2}$s$^{-1}$ (also usually expressed in femtobarns) which is very useful in measurements involving high-energy QCD but has its problems in the lower energy scales. An important related quantity is also integrated luminosity which is an integral over time of luminosity and it is mainly used to describe the efficiency and performance of the detector and is expressed in inverse of the cross section. Usually among the many proton-proton collisions in one bunch crossing, one contains interesting information. The rest of the collisions is called "pileup" which will be addressed in later sections. \\
\\
Since the CMS detector is cylindrical in shape, it is logical to use cylindrical coordinates to describe the system. Suppose that the beam travels in direction $\hat{z}$, the azimuthal angle $\phi$ is not interesting beacause the observed process will always be invariant to the rotation of the detector. The angle $\theta$ usually represents the direction of the observed particle with respect to the beam direction but here it is more useful to use pseudorapidity, $\eta = - ln{tan{\theta/2}}$ since the difference in pseudorapidity is Lorentz invariant. To finalize the construction of the 4-vector of an object in the detector the transverse momentum $p_T$ is needed and represents the momentum of the object in the plane transverse to beam direction. Including all previously defined physical quantities ($p_t, \phi, \eta, m$) an object inside the detector is fully defined. \\
Since transverse variables are used for the description of processes it is also necessary to define the missing transverse energy which is expected to occur in all hadron collider processes and needs to be accounted for in all measurements. Assuming the direction of the beam is $\hat{z}$ the missing transverse energy vector is defined as \cite{r17}:

\begin{equation}
\textbf{E}^{miss}_{T}=-\sum_{i}\textbf{p}_T(i)
\end{equation}

where the sum is over all transverse momenta of observed particles in the detector.\\
\\
The LHC is colliding particles at very high energies, raching energies of 13 TeV per collision in the latest upgrades. Interactions at such high energies will have consequences on kinematics of the process, primarily when the condition $p_T$(object) $>>$ $m$(object) is met, which is expected for energies in the TeV energy scale, the products will be boosted and objects that decay from them are expected to be collimated.

\newpage

\section{Monte Carlo Simulations and Calcuations}

...

\section{Terminology}

This will probably not be a section by itself it is here temporarily.

- "Data" = Set of information from "events" from real LHC collisions \\
- Information from events are stored in ROOT files in many formats called "Data formats".  \\
- One ROOT file can contain main events. \\
- The Data format dictates which information are stored, from very complicated and detailed list of information to very simple and a small subset of the total information. \\
- In physics analyses, we usually store information on the 4-momentum of "physics objects". \\
- "Physics objects" - Electrons, muons, tau, photons, jets, missing transverse energy. \\
 - these are stored plus detailed information also on the particles clustered into jets. \\
- There are also some other info used for identifying particles.

\section{Analyzing Jets}

- Average fractional difference between a jet’s transverse momentum and that of the original parton is very important. \\
- "splash-out" gluon radiated beyond reach of R (jet definition) reduces jet energy \\
- fractional difference comes from that \\
- because of pile-up and UE contributions jets will have apparent more energy than partons \\
- DGLAP equation from qcd \\
- Small R limit fractional difference can be calculated by integration (perturbative calculation) depending on chosen agorithm ($f_{arg}$ function = 1 for anti-kt) \\
- jet mass distribution $d\sigma/(dM)^2$ at LO divergates for small masses \\
- non perturbative properties of jet - divided - hadronisation and underlying event (multiple low pt processes in hadron remnants pp or $\gamma p$)  \\
- hard to separate \\
- in small R limit you can take pertu. integrals and substitute intergrals that go over $\alpha S$ with a non pertubative variation and get results that depend on Landau scale \\
- Landau scale $\Gamma \approx 0.6 $GeV, universality \\
- Milan factor - corrections taht arise when one considers two non pert gluons instead of one, it is universal \\
- even if non pert effects are rather small they have significant impact on cross section value \\
- non pert corrections also able to calculate simmilarly (Landau method) \\
- UE induces extra amount of transverse momentum per unit rapidity $\Lambda UE$, contribution to jet pt prop to $R^2$ also change to square mass \\
- LHC large UE contribution \\
- important to understand how jets are affected by low pt noise that is roughly uniform in rapidity \\
- Jet area, how deeply is a jet definition affected by soft radiation, passive for pointlike radiation \\
- for pointlike, ghost particle intro. with infinitesimally pt and jet area defined over region in which ghost is clustered with the jet \\
- active area - probability of diffuse radiation, a lot of uniformly distr. ghost particles (ensamble), then average over ensambles \\
- UE is somewhere inbetween \\
- ANTI-KT PASSIVE IS IDENTICAL TO ACTIVE AND INDEPENDENT OF GHOST ANSAMBLE \\
- ghosts also cluster inbetween themselves \\
- real jets complex, on first approx. one can calculate jet area when soft gluon added at certain theta \\
- ANTI-KT JET AREA IS IR SAFE \\
- back-reaction, when jet of area a is in bakground of soft gluons with finite pt not only will some end up in jets but it will contaminate the algorithm of clustering so it will not be precise like before \\
- pointlike can be calculated, diffuse not really because asymptotic behaviour \\
- usually small compared to UE or pileup \\
- ANTI-KT BACKREACTION IS REAAAALLY SUPPRESSED - essentially zero \\
- choose algortihm that minimises hadronisation and UE contributions \\
- one deduces a value of R that minimises the squared sum of hadronisation and UE pieces \\
- gluon jets and high pt prefer large R values \\
- Monte Carlo hard patrons, let them shower and hadronise, analyze jets if they match the original patrons \\
- number of jets equal to partons, angular distance between jets and partons, pt difference between jets and partons \\
- analyze which algorithm gives best signal to background ratio \\
- position and width of mass peak matters \\
- the larger the importance of perturbative gluon radiation, the larger the preferred R value \\
- same R value differences in algortihms - maybe related to jet areas \\
- significance of any signal $S/sqrt{B}$ is proportional to $1/sqrt{Q}$ since  amount of background basically scales as the width of the window that comes out of the quality \\
- measure $\rho_L$ extra factor in luminosity needed to see the signal with given significance when using one jet definition relative to another \\
- impact of the jet algorithm choice is greatest at large mass scales \\
- variable R can be used since it does make some improvements in signal reconstruction, $R \sim cosh{\Delta{y}/2}$, $\sim 10\%$ improvement \\
- dijet backgrounds improved only when cut which requires that the energy be deposited centrally within the jet was introduced \\
- pile-up roughly modifies a jets pt \\
- depends on $\rho$, $\sigma$, A and $\Delta_pt$ but $\rho$ dependance can be removed by running the algorithm but it is non-trivial because the hard and pileup needs to be distinguished \\
- important because analyses that use data taken at different luminosities can successfully use a common jet algorithm independently of the pileup \\
- identifiying hadronic decays of boosted heavy particles in two categories: two-pronged decays (EW bosons) and three-pronged(top quarks) \\
- even for masless partons QCD branching generates significant number of jets with masses, QCD mass distribution LO can be approx. \\
- Y-splitter - distribution of kt distance $d_ij$ between the two W subjects is close to the mass of W in W decays but tended to have lower values in generic massive jets \\
- quasi-colinear splitting into two objects total mass is $m^2 \simeq p_{ti} p_{tj} (\Delta R_{ij})^2$ \\
- higgs boson decays fairly uniform distribution in z \\
- for a fixed mass window, the background will have lower dij values than the signal \\
- cut eliminates logarithm or in essence QCD background which can be even calculated analitycaly \\
- if one uses subjets for reconstruction, loss of squared mass will be dominated by perturb. gluon radiation \\
- if one uses a single jet then loss is dominated by UE and pileup \\
- optimising mass resolution - minimising both effects

\section{Basic plots}

\section{Plots with neutrinos}

\section{Fat Jets}

The soft-drop algorithm removes softer constituents of a jet and primarily works around the equation: 

\begin{equation}
min(p_{T1},p_{T2})/(p_{t1}+p_{T2}) > z_{cut}(\Delta R_{12}/R)^\beta
\end{equation} 

Here beta is an angular exponent and if it goes to infinity the jet is ungroomed. If $\beta > 0$ it removes soft radiation while maintaining some fraction of it and is a grooming tool, if $\beta < 0$ it removes both soft and collinear radiation. If $\beta= 0$ the soft drop is also a tagger and is a generalisation of the above mentioned mass drop tagger (of maybe some variation of it).

How it works:
- breaks the jet into two subjets and labels them as j1 and j2
- tests if the jets pass the above condition (there is also one for the mass: max(m1,m2)<mu m)
- if yes, j is the final soft-drop jet
- if not, j is equal to the subjet with higher pt value, iterates
- if j can no longer be declustered, there are two options: tagging mode, remove j from consideration or grooming mode, leave j as the final soft-drop jet.

\section{Higgs decay to $b\bar{b}$}

Reconstructing color-singlet $q\bar{q}$ states with small cone sizes will cause greater contributions from pertubative gluon radiation resulting in loss of mass resolution. Here it is useful to use a filtering technique to prevent this. A new (smaller) value of R is chosen that is equal to min($0.3, R_{b\bar{b}}/2$) so only the two hardest components remain (which will reduce effects like UE).

Looking at data from previous references, a choice of values for the parameters of the soft-drop algorithm is primarily $z=0.1$ and $\beta=0$. Going back to the condition equation this would simplify the condition and it is exactly the histogram that is plotted in fig. So in the histogram above, subjets above the fraction value 0.1 would pass the condition and would be labeled as final jets (of course if they are within the Higgs mass range).  \\
The histograms that use the scalar sum in the denominator of the formula for the ratio of the subjets compared to the ones that use the vector sum are of very similar values. The reason for that is because the angle between the vectors (subjets) is very small (close to 0) which is highly expected because the decay process is Lorentz boosted so the jets are collimated and precisely the maximum $\Delta R$ between the subjets can be 0.8. 

\newpage

\bibliography{References}
\begin{thebibliography}{1}

\bibitem{r1} The ATLAS Collaboration, Observation of a new particle in the search for the Standard Model Higgs boson with the ATLAS detector at the LHC, Phys.Lett. B716, 1-29, 2012

\bibitem{r2} The CMS Collaboration, Observation of a new boson at a mass of 125 GeV with the CMS experiment at the LHC, Phys. Lett. B716, 30, 2012  

\bibitem{r3} Schwartz, M, D 2014, Quantum Field Theory and the Standard Model, Cambridge University Press, New York

\bibitem{r4} Peskin, M, E, Schroeder, D, V 1995, Introduction to Quantum Field Theory, Perseus Books Publishing, Massachusetts
 
\bibitem{r5} The ATLAS Collaboration, Measurement of the Higgs boson mass in the H$\rightarrow ZZ^* \rightarrow 4l$ and $H \rightarrow \gamma\gamma$ channels with $\sqrt{s}=13$  TeV $pp$ collisions using the ATLAS detector, Phys.Lett.B 784, 345-366, 2018

\bibitem{r6} CMS Collaboration, Measurements of properties of the Higgs boson decaying into the four-lepton final state in pp collisions at $\sqrt{s}=13$ TeV, JHEP 11, 047, 2017

\bibitem{r7} ATLAS and CMS Collaborations, Combined Measurement of the Higgs Boson Mass in $pp$ Collisions at $\sqrt{s}=7$ and $8$ TeV with the ATLAS and CMS Experiments, Phys.Rev.Lett. 114, 191803, 2015

\bibitem{r8} The CMS Collaboration, Search for a massive resonance decaying to a pair of Higgs bosons in the four b quark final state in proton–proton collisions at $\sqrt{s}=13$ TeV, Physics Letters B 781, 244-269, 2018

\bibitem{r9} Dolan M., Englert C., Spannowsky M., Higgs self-coupling measurements at the LHC, arXiv:1206.5001 [hep-ph], 2012

\bibitem{r10} M. Tanabashi et al. (Particle Data Group), Status of Higgs Boson Physics, Phys. Rev. D 98, 030001, 2018

\bibitem{r11} Agashe K., Davoudiasl H., Perez G., Soni A., Warped Gravitons at the LHC and Beyond, Phys.Rev.D 76, 036006, 2007

\bibitem{r12} Randall L., Sundrum R., Large mass hierarchy from a small extra dimension, Phys. Rev. Lett. 83, 3370–3373, 1999

\bibitem{r13} Randall L., Sundrum R., An alternative to compactification, Phys. Rev. Lett. 83, 4690–4693, 1999

\bibitem{r14} Csaki C., Graesser M. L., Kribs G. D., Radion Dynamics and Electroweak Physics, Phys.Rev.D 63, 065002, 2001

\bibitem{r15} http://cms.web.cern.ch/news/cms-detector-design

\bibitem{r16} http://cds.cern.ch/record/2204863/

\bibitem{r17} M. Tanabashi et al. (Particle Data Group), Phys. Rev. D 98, 030001, 2018

\end{thebibliography}
%\bibliographystyle{report}

%\newpage\phantom{blabla} 

\end{document}
